"""
Python3.8
Gemaakt door: Ernest Lassman
Naam programma: create_playlist_in_database.py
Laatst gewijzigd: 02/09/2020
Dit programma creert een nieuwe afspeellijst in een database.
De inhoud van de afspeellijst staat in een tekstbestand geschreven.
De gebruiker geeft de naam van het te importeren tekstbestand en een naam voor de nieuwe afspeellijst.
Als er meerdere liedjes worden gevonden met eenzelfde naam, krijgt de gebruiker een keuzemenu met 4 opties.
Vervolgens wordt de nieuwe afspeellijst aangemaakt in de database.
"""


import sqlite3
import sys


counter = 0  # Index van het zoekwoord in een lijst
countfile = 0  # Het aantal regels met één zoekwoord in de file
countplaylist = 1  # Geeft een nummer aan elke track in de keuzemenu
conn = sqlite3.connect("chinook.db")  # Database verbinding
cur = conn.cursor()


#   Een bestand met de namen van liedjes wordt gelezen
def file_reader(filename=input("Geef naam van het te importeren bestand:")):
    global countfile
    with open(filename, "r") as a:
        regel = a.read().split("\n")
    # Maakt een lijst van alle inputs uit het bestand
    wholefile = [[str("%" + val + "%")] for val in regel]
    # Nieuwe lijst zonder lege regels enkel met zoekwoorden uit het bestand
    partfile = [x for x in wholefile if x != ["%%"] if x != ["% %"]]
    countfile = len(partfile)
    if countfile == 0:  # Als het bestand leeg is, stopt het programma met de melding
        sys.exit("Geen zoekwoord gevonden")
    return partfile


#   Maken van een afspeellijst in de database
class CreatingPlaylist:
    def __init__(self):
        self.naampl = input("Geef naam van de playlist: ")  # Vraagt de naam van de afspeellijst
        #   Neemt de naam van alle afspeellijsten in de database
        self.check = cur.execute("SELECT Name FROM playlists WHERE Name = ?", (self.naampl,))

    #   Verifieert of de naam van de gekozen afspeellijst alreeds bestaat in de database
    def naam_playlist(self):
        for y in self.check:
            if y[0] == self.naampl:  # Verifieert of de gekozen afspeellijst alreeds bestaat in de database
                sys.exit("Deze playlist bestaat al")  # Print de fout en stopt het programma
        cur.execute("INSERT INTO playlists (Name) VALUES(?)", (self.naampl,))  # Maakt de nieuwe afspeellijst aan
        print("--- Start import van playlist ---")

    #   Geeft de naam van de afspeellijst terug
    def getnaam(self):
        return self.naampl


#   Gebruiker kan een liedje kiezen uit de keuzenlijst
def kies_optie():
    option = 0
    trail = 0
    while not 4 >= trail >= 1:
        try:
            trail = int(input("Uw keuze: "))  # De gebruiker moet een getal ingeven
            if trail <= 0 or trail >= 5:  # Het getal is groter of kleiner dan mogelijke opties
                print("Kies een getal van 1 en 4")
            else:
                option = trail - 1
        except ValueError:  # Geen cijfer ingegeven door de gebruiker
            print("Dit is geen geheel getal")
    return option


def check_database():
    global countfile
    global counter
    global countplaylist
    #   De database wordt doorzocht per zoekwoord uit het bestand.
    while countfile:
        #  Het aantal liedjes in de database per zoekwoord
        counting = cur.execute("SELECT COUNT(*) FROM (SELECT * FROM tracks WHERE Name LIKE ?)",
                               allvals[counter])
        allcounting = counting.fetchall()
        # Kijkt na of er meer dan 4 liedjes per zoekwoord zijn gevonden in de database
        if allcounting[0][0] > 4:
            rounds = 4
        else:
            rounds = allcounting[0][0]
        countrounds = 0  # Index van het elementen die we nodig hebben in de lijst van gegevens uit de database
        displayallvals = allvals[counter]  # Zoekwoorden uit het bestand
        # De database wordt bewerkt en de playlist wordt aangevuld
        while rounds:
            #   Één liedje wordt meteen ingevoer in de afspeellijst van de database
            if allcounting[0][0] == 1:
                data = cur.execute("SELECT * FROM tracks WHERE Name LIKE ?", allvals[counter]).fetchone()
                trackid = data[countrounds]
                rounds -= 1
                plid = cur.execute("SELECT playlistId FROM playlists "
                                   "WHERE Name = ? ", (naamterug,)).fetchall()
                cur.execute("INSERT INTO playlist_track (PlaylistId, TrackId) VALUES (?, ?)", (plid[0][0], trackid))
            #   Meerdere liedjes gevonden
            elif allcounting[0][0] > 1:
                # Gegevens van 4 liedjes selecteren uit de database
                data = cur.execute("SELECT artists.ArtistId, tracks.Name, tracks.AlbumId, tracks.trackId "
                                   "FROM tracks "
                                   "INNER JOIN artists ON albums.ArtistId = artists.ArtistId "
                                   "INNER JOIN albums ON tracks.AlbumId = albums.AlbumId "
                                   "WHERE tracks.Name LIKE ? "
                                   "ORDER BY tracks.Name DESC", (allvals[counter])).fetchmany(4)
                # Sorteren van de keuzenlijst op basis van ArtistId
                sortdata = sorted(data, key=lambda y: y[0])
                track = sortdata[countrounds][1]
                albumid = sortdata[countrounds][2]
                # We zoeken de artistnaam voor een bepaald liedje
                tupleartist = cur.execute("SELECT artists.Name, artists.ArtistId "
                                          "FROM artists "
                                          "INNER JOIN albums ON artists.ArtistId = albums.ArtistId "
                                          "INNER JOIN tracks ON albums.AlbumId = tracks.AlbumId "
                                          "WHERE albums.AlbumId = ? ", (albumid,)).fetchone()
                artist = tupleartist[0]
                print("{}\t\t{}\t\t{}".format(countplaylist, track, artist))  # Weergave van de keuzemenu
                # Na het selecteren van alle elementen in de keuzemenu
                if rounds == 1:
                    choice = kies_optie()  # Index van het gekozen liedje uit de keuzemenu
                    # PlaylistId voor de nieuwe afspeellijst
                    plid = cur.execute("SELECT playlistId FROM playlists "
                                       "WHERE Name = ? ", (naamterug,)).fetchall()
                    # Liedje wordt gelinkt aan afspeellijst
                    cur.execute("INSERT INTO playlist_track (PlaylistId, TrackId) VALUES (?, ?)",
                                (plid[0][0], sortdata[choice][3]))
                rounds -= 1
                countrounds += 1
                countplaylist += 1
        #   Geen liedjes gevonden met de title
        if allcounting[0][0] == 0:
            txt = displayallvals[0]
            newdist = txt.strip("%")
            print("--- Geen tracks gevonden voor {} ---".format(newdist))

        countfile -= 1
        counter += 1


if __name__ == "__main__":
    try:
        allvals = file_reader()
        pl = CreatingPlaylist()
        pl.naam_playlist()
        naamterug = pl.getnaam()
        print("Maak een keuze uit de volgende tracks")
        check_database()
        print("--- Import van playlist gereed ---")
    except sqlite3.OperationalError:  # Als de database niet bestaat stopt het programma
        sys.exit("Data niet gevonden")
    except FileNotFoundError:  # Als het bestand niet bestaat stopt het programma
        sys.exit("Bestand niet gevonden")
    except PermissionError:  # Zonder toegang tot het bestand stopt het programma
        sys.exit("Geen toegang tot dit bestand")


# conn.commit()  # Wijzigingen definitief in de database
conn.close()  # sluit database verbinding
